<?php namespace lucatomasi\Notify;

use Exception;
use InvalidArgumentException;
use lucatomasi\XFunc\XFunc;

/**
 * @author Luca Tomasi | lucatomasi77@gmail.com
 * @version 3.0.6
*/


/**
 * Class Notify
 * @package lucatomasi\Notify
 */
class Notify {

    /**
     * @var array $success Holder for success messages
     */
    public static $success = [];

    /**
     * @var array $error Holder for error messages
     */
    public static $error   = [];

    /**
     * @var array $info Holder for info messages
     */
    public static $info    = [];

    /**
     * @var array $warning Holder for warning messages
     */
    public static $warning = [];

    /**
     * @var array $config Default configurations
     */
    private static $config = [
        'debug' => false,
        'hideCountdown' => 7000,
        'animSpeed' => 700,
        'colors' => [
            'success' => '#82a852',
            'error' => '#eb4034',
            'warning' => '#fcba03',
            'info' => '#4287f5',
        ]
    ];

    /**
     * Stores the message to be printed on page load
     * @param string $msg The message to be saved
     * @param string $type The type of message 'success', 'error', 'info' or 'warning'
     * @param string|null $elementID The id of the HTML-Element to attach the message to
     * @throws Exception
     */
    public static function add(string $msg, string $type = 'error', string $elementID = NULL): void {
        if (!property_exists(get_called_class(), $type)) {
            throw new InvalidArgumentException("The message type [{$type}] is invalid use one of the following: (success, error, warning, info)");
        }
        if (is_null($elementID)) {
            self::${$type}[] = $msg;
        } else {
            self::${$type}[] = [$msg, $elementID];
        }
    }

    /**
     * Returns the required CSS recourses
     * @return string[]
     */
    public static function getCssFiles(): array {
        $url = XFunc::getPathOf(__FILE__)['url'];
        return [$url . 'utils/css/notify.css'];
    }

    /**
     * Returns the required JS recourses
     * @return string[]
     */
    public static function getJsFiles(): array {
        $url = XFunc::getPathOf(__FILE__)['url'];
        return [$url . 'utils/js/Notify.js'];
    }

    /**
     * Returns required HTML, put this at the end of the body after jQuery
     * @param array $config
     * @return string
     */
    public static function print(array $config = []): string {
        $messages = json_encode([
            'success' => self::$success,
            'error' => self::$error,
            'warning' => self::$warning,
            'info' => self::$info
        ]);

        foreach ($config as $key => $c) {
            self::$config[$key] = $c;
        }

        $url = XFunc::getPathOf(__FILE__)['url'];

        self::$config['utilsURL'] = $url . 'utils/';

        $notifyConfig = json_encode(self::$config);

        return <<<heredoc
            <script>
                let NOTIFY = new Notify({$messages}, {$notifyConfig});
            </script>
heredoc;

    }

}