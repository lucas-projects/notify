/**
 * @author Luca Tomasi | lucatomasi77@gmail.com
 */
class Notify {

    constructor(messages, config) {
        this.config = config;

        this.stickyNotifications = [];

        let positionF = () => {
            for (let n of this.stickyNotifications) {
                this.setStickyNotificationPos(n.n, n.inp, false);
            }
        };

        $(window).scroll(positionF);

        $(window).resize(positionF);


        $('body').prepend('<div id="notify-holder-outer"></div>');
        this.holder = $('#notify-holder-outer');

        for (let type in messages) {
            if (messages.hasOwnProperty(type)) {
                for (let message of messages[type]) {
                    if (typeof message === 'object') {
                        this.add(message[0], type, message[1]);
                    } else {
                        this.add(message, type);
                    }
                }
            }
        }
    }

    add(msg, type = 'error', elementID = null) {

        let nID = 'notification-id-' + Math.random().toString(36).substring(7);
        let nHTML =
            '<div id="' + nID + '"  class="notify-notification' + ((elementID !== null) ? '-sticky' : '')  + '">' +
            '    <img src="' + this.config.utilsURL + 'img/' + type + '.svg" alt="' + type.charAt(0).toUpperCase() + type.slice(1) + '">' +
            '    <div>' + msg + '</div>' +
            '</div>';
        this.holder.append(nHTML);


        let n = $('#' + nID)
            .css({
                'background-color': this.config.colors[type],
            });

        if (elementID !== null) {

            let inp = $('#' + elementID);
            if (!inp.length) {
                let msg = 'Element with id [' + elementID + '] was not found.';
                if (this.config.debug === true) throw new Error(msg);
                console.error(msg);
                return;
            }

            n.css({'border-color': this.config.colors[type]});

            inp.hover(() => {
                let pos = inp.offset();
                n.css({
                    width: inp.outerWidth(),
                    top: pos.top - $(window).scrollTop() + inp.outerHeight(),
                    left: pos.left
                });

                this.setStickyNotificationPos(n, inp, true);
            }, () => {
                n.css('height', '0');
            });

            this.setStickyNotificationPos(n, inp, false);
            this.stickyNotifications.push({n: n, inp: inp});

        } else {
            n.animate({
                'opacity': 1
            }, this.config.animSpeed);

            if (this.config.hideCountdown !== 0) {
                setTimeout(() => {
                    this.hideNotification(n);
                }, this.config.hideCountdown);
            }

            n.click(() => {
                this.hideNotification(n);
            });
        }
    }

    setStickyNotificationPos(n, inp, show = true) {
        let pos = inp.offset();
        if (show) n.css('height', 'auto');
        n.css({
            width: inp.outerWidth(),
            top: pos.top - $(window).scrollTop() + inp.outerHeight(),
            left: pos.left
        });
    }


    hideNotification(n) {
        n.animate({
            opacity: 0
        }, {
            duration: this.config.animSpeed,
            complete:  () => { n.remove(); }
        });
    }

    clearNotifications() {
        for (let n of this.stickyNotifications) {
            this.hideNotification($(n.n));
        }
    }

}
